﻿using SQLite;

namespace Contact.Model
{
    public class ContactItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Mail { get; set; }
        public string Description { get; set; }
    }
}

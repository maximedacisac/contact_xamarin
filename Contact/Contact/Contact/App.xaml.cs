﻿using Contact.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Contact
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new ContactListPage())
            {
                BarTextColor = Color.White,
                BarBackgroundColor = Color.DarkGreen
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

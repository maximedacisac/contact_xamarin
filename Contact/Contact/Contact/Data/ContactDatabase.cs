﻿using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contact.Model;

namespace Contact.Data
{
    public class ContactDatabase
    {
        static SQLiteAsyncConnection Database;

        public static readonly AsyncLazy<ContactDatabase> Instance = new AsyncLazy<ContactDatabase>(async () =>
        {
            var instance = new ContactDatabase();
            CreateTableResult result = await Database.CreateTableAsync<ContactItem>();
            return instance;
        });

        public ContactDatabase()
        {
            Database = new SQLiteAsyncConnection(DatabaseInitializer.DatabasePath, DatabaseInitializer.Flags);
        }

        public Task<List<ContactItem>> GetItemsAsync()
        {
            // return Database.Table<ContactItem>().ToListAsync();
            return Database.QueryAsync<ContactItem>("SELECT * FROM [ContactItem] ORDER BY [FirstName] ASC, [LastName] ASC");
        }

        public Task<ContactItem> GetItemAsync(int id)
        {
            return Database.Table<ContactItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(ContactItem item)
        {
            if (item.ID != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(ContactItem item)
        {
            return Database.DeleteAsync(item);
        }
    }
}

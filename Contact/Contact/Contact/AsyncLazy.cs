﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Contact
{
    public class AsyncLazy<T>
    {
        readonly Lazy<Task<T>> instance;

        public AsyncLazy(Func<T> func)
        {
            instance = new Lazy<Task<T>>(() => Task.Run(func));
        }

        public AsyncLazy(Func<Task<T>> func)
        {
            instance = new Lazy<Task<T>>(() => Task.Run(func));
        }

        public TaskAwaiter<T> GetAwaiter()
        {
            return instance.Value.GetAwaiter();
        }
    }
}

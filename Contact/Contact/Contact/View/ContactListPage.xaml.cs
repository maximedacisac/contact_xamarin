﻿using Contact.Data;
using Contact.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Contact.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactListPage : ContentPage
    {
        public ContactListPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            ContactDatabase database = await ContactDatabase.Instance;
            listView.ItemsSource = await database.GetItemsAsync();
        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ContactDetailPage
            {
                BindingContext = new ContactItem()
            });
        }

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new ContactDetailPage
                {
                    BindingContext = e.SelectedItem as ContactItem
                });
            }
        }

        async void OnDelete(object sender, EventArgs e)
        {
            ContactDatabase database = await ContactDatabase.Instance;

            var swipeview = sender as SwipeItem;
            var contact = (ContactItem)swipeview.CommandParameter;

            await database.DeleteItemAsync(contact);

            listView.ItemsSource = null;
            listView.ItemsSource = await database.GetItemsAsync();
        }
    }
}
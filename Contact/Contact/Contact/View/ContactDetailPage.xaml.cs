﻿using Contact.Data;
using Contact.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Contact.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailPage : ContentPage
    {
        public ContactDetailPage()
        {
            InitializeComponent();
        }
        async void OnSaveClicked(object sender, EventArgs e)
        {
            var contact = (ContactItem)BindingContext;
            if(!String.IsNullOrWhiteSpace(contact.FirstName) && !String.IsNullOrWhiteSpace(contact.LastName) && !String.IsNullOrWhiteSpace(contact.PhoneNumber) && !String.IsNullOrWhiteSpace(contact.Mail))
            {
                ContactDatabase database = await ContactDatabase.Instance;
                await database.SaveItemAsync(contact);
                await Navigation.PopAsync();
            }
            else
            {
                errorField.IsVisible = true;
            }
        }

        async void OnDeleteClicked(object sender, EventArgs e)
        {
            var contact = (ContactItem)BindingContext;
            ContactDatabase database = await ContactDatabase.Instance;
            await database.DeleteItemAsync(contact);
            await Navigation.PopAsync();
        }
    }
}
Projet Xamarin M2.

Application de gestion de contact avec persistance en local.

Fonctionnalité : 
 - Ajout d'un contact
 - Modification d'un contact
 - Suppression d'un contact (depuis la liste ou swipant sur la gauche ou depuis l'écran de modification)
 - Visualisation des contacts depuis une liste.

Pour lancer le projet, il faut installer Visual Studio. Il faut aussi installer un emulateur Android mais il y en a un disponible avec Visual Studio.
